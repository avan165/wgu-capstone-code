import numpy as np
import math
import random
import csv
import matplotlib.pyplot as plt


class NeuralNetwork():
    mutation_rate = 0.01
    starting_mutation_rate = 0.01
    mutation_rate_max = 0.25
    def __init__(self, low_end_weight=-2.0, high_end_weight=2.0):
        self.layers = [12, 9, 9, 1]
        self.fitness = 0.0
        self.population_fitness_percentage = 0.0   # the percent of the total population fitness the network has
        self.population_percentage_position = 0.0   # the high limit of the fitness percentage of the population
        self.low_end_weight = low_end_weight
        self.high_end_weight = high_end_weight
        num_weights = 0
        for x in range(len(self.layers) - 1):
            num_weights += self.layers[x] * self.layers[x+1]
        self.weights = np.random.uniform(low_end_weight, high_end_weight, num_weights)
        
    def feed_forward(self, input_variables):
        curr_layer_values = input_variables
        weight_index = 0
        for x in range(1, len(self.layers)):   # iterating through the layers
            next_layer_values = np.zeros(self.layers[x])    # create a numpy array with length of next layer
            for y in range(self.layers[x]):   # iterating through the next layer's nodes
                for z in range(self.layers[x-1]):   # iterating through the node's input weights
                    next_layer_values[y] += curr_layer_values[z] * self.weights[weight_index]
                    weight_index += 1
                next_layer_values[y] = activation_function(next_layer_values[y])   # call activation function after all inputs are processed
            curr_layer_values = next_layer_values
        return curr_layer_values   # the output node's values
        
    def set_fitness(self, val):
        self.fitness = val
        
    @classmethod
    def increase_mutation_rate(cls):
        if cls.mutation_rate < cls.mutation_rate_max:
            cls.mutation_rate += 0.01
         
    @classmethod
    def reset_mutation_rate(cls):
        cls.mutation_rate = cls.starting_mutation_rate
  

  
def calculate_fitness(output, desired_output):
    diff = abs(output - desired_output)
    fitness = 1.0 / math.pow((diff + 1.0), 2.0)
    return fitness
        
def breed(network1, network2):   # function to create a network similar to the two parent networks
    child_network = NeuralNetwork()   
    for x in range(len(child_network.weights)):
        rand_val = random.random()
        if rand_val >= (1.0 - NeuralNetwork.mutation_rate):
            child_network.weights[x] = random.uniform(network1.low_end_weight, network1.high_end_weight) 
        elif rand_val >= (0.5 - NeuralNetwork.mutation_rate + 0.01):
            child_network.weights[x] = network1.weights[x]
        else:
            child_network.weights[x] = network2.weights[x]
            
    return child_network
        
        
        
def activation_function(value):   # RELU
    return max(0, value)
        
def genetic_algorithm(population_size, num_epochs, housing_data, initial_population=[]):
    
    # create the initial population
    if len(initial_population) > 0:   # add a pre-trained population if it is given
        population = initial_population
    else:   
        population = np.zeros(population_size, dtype=object)
        for i in range(population_size):
            population[i] = (NeuralNetwork()) 
    
    
    epoch = 1
    best_fitness = 0.0
    best_fitness_10_ago = 0.0
    while True:
        index_of_best_network = 0
        for index in range(len(population)):
            fitness_sum = 0.0
            for data_line in housing_data:
                output = population[index].feed_forward(data_line[1:])[0]
                fitness_sum += calculate_fitness(output, data_line[0])
            fitness_avg = fitness_sum / len(housing_data)
            population[index].set_fitness(fitness_avg)   # set the network's fitness
            if population[index].fitness > population[index_of_best_network].fitness:
                index_of_best_network = index
        print('EPOCH: ', epoch, '   Network #', index_of_best_network, ' fitness: ', population[index_of_best_network].fitness)   # print best network -- make so it moves on definitly2
        
        best_fitness = population[index_of_best_network].fitness
        if epoch % 10 == 0 and epoch != 0:
            if best_fitness == best_fitness_10_ago:   # no progress made in 20 epochs
                NeuralNetwork.increase_mutation_rate()
            else:
                best_fitness_10_ago = best_fitness
                NeuralNetwork.reset_mutation_rate()
            print(NeuralNetwork.mutation_rate)
                
        if epoch >= num_epochs:
            return population[index_of_best_network]   # return best network after training on last epoch
        # breed new population 
        population_fitness_sum = 0
        for network in population:
            population_fitness_sum += network.fitness
            
        if population_fitness_sum <= 0.0000001:   # skip epoch and create new networks if there are no fit networks
            print('SKIPPED EPOCH: ', epoch)
            for i in range(population_size):
                population[i] = (NeuralNetwork())  
            continue
   
        total_percentage = 0.0
        for network in population:   # determining the high limit of each networks fitness percentage for breeding pool
            network.population_fitness_percentage = network.fitness / population_fitness_sum
            total_percentage += network.population_fitness_percentage
            network.population_percentage_position = total_percentage
        population[population_size - 1].population_percentage_position = 1.0   # to ensure the value is 1.0
        
        children_population = np.zeros(population_size, dtype=object)   # the next generation of networks
        for i in range(10):
            children_population[i] = population[index_of_best_network]   # add 10 of best network to next generation
        for i in range(10, population_size):
            parent1_index = binary_network_search(random.random(), population)
            parent2_index = binary_network_search(random.random(), population)
            parent_network1 = population[parent1_index]   # pick a parent network based on fitness
            parent_network2 = population[parent2_index]   # pick a parent network based on fitness
           
            child_network = breed(parent_network1, parent_network2)
            children_population[i] = child_network
            
    
        population = children_population   # set the population to be the child population
        epoch += 1
    return None   # should not reach.
    
    
def binary_network_search(network_decimal, networks):
    low_index = 0
    high_index = len(networks) - 1
    while low_index < high_index:
        mid_index = (low_index + high_index) // 2
        if network_decimal < networks[mid_index].population_percentage_position:
            high_index = mid_index
        elif network_decimal > networks[mid_index].population_percentage_position:
            low_index = mid_index + 1
        else:
            return mid_index
    return low_index
       
    
def load_file_data():
    data_list = []
    with open('Housing.csv') as housing_file:
        reader = csv.reader(housing_file)
        next(reader)   # skip the labels line
        for line in reader:
            modified_list = []
            price = float(line[0]) / 1000000.0   # price in millions of dollar
            modified_list.append(price)   
            sqfoot = float(line[1])
            sqfoot_normalized = (sqfoot - 1650.0) / (15600.0 - 1650.0)
            modified_list.append(sqfoot_normalized)
            modified_list.append(int(line[2]))   # num bedrooms
            modified_list.append(int(line[3]))   # num bathrooms
            modified_list.append(int(line[4]))   # num stories
            modified_list.append((1 if line[5] == 'yes' else 0))   # has mainroad access
            modified_list.append((1 if line[6] == 'yes' else 0))   # has guest room
            modified_list.append((1 if line[7] == 'yes' else 0))   # has basement
            modified_list.append((1 if line[8] == 'yes' else 0))   # has hotwaterheating
            modified_list.append((1 if line[9] == 'yes' else 0))   # has airconditioning
            modified_list.append(int(line[10]))   # num parking spaces
            modified_list.append((1 if line[11] == 'yes' else 0))   # preferred area
            val = -1.0
            if line[12] == 'furnished':
                val = 1.0
            elif line[12] == 'semi-furnished':
                val = 0.5
            else:
                val = 0.0
            modified_list.append(val)   # furnished status
            data_list.append(modified_list)
    return data_list
    
def write_weights_file(network):
    file = open('best_network_weights.txt', 'w')
    for weight in network.weights:
        file.write(str(weight) + '\n')
    file.close()
    
    
def load_network(filename):
    file = open(filename, 'r')
    network = NeuralNetwork()  
    
    for index, line in enumerate(file):
        network.weights[index] = float(line)
    file.close()
    return network
    
def scatter_plot():
    data = load_file_data()
    x = np.zeros(len(data))   # square footage
    y = np.zeros(len(data))   # house price
    for index, data_sample in enumerate(data):
        y[index] = data_sample[0]
        x[index] = (data_sample[1] * 13950.0) + 1650.0   # unnormalize the data
    
    plt.xlabel('Square Footage of House')
    plt.ylabel('Price (In Millions of Dollars)')
    plt.scatter(x, y)
    plt.show()
    
def pie_chart():
    data = load_file_data()
    num_stories_data = np.zeros(len(data))   # min: 1   max: 4
    for index, data_sample in enumerate(data):
        num_stories_data[index]  = data_sample[4]

    unique, counts = np.unique(num_stories_data, return_counts=True)
    plot_dict = dict(zip(unique, counts))
    plot_list = list(plot_dict.values())
    plot_labels = list(map(int, list(plot_dict.keys())))
    plt.pie(plot_list, labels = plot_labels, startangle = 90, autopct = '%.4f')
    plt.legend(title = 'Number of Stories')
    plt.show()
    
def bar_chart():
    data = load_file_data()
    with_hotwater_avg_price = 0.0
    without_hotwater_avg_price = 0.0
    with_ac_avg_price = 0.0
    without_ac_avg_price = 0.0
    num_houses_with_hotwater = 0
    num_houses_with_ac = 0
    for index, data_sample in enumerate(data):
        price = data_sample[0] * 1000000.0
        if data_sample[8] == 1:   # has hotwater heating
            with_hotwater_avg_price += price
            num_houses_with_hotwater += 1
        else:
            without_hotwater_avg_price += price
        
        if data_sample[9] == 1:   # has ac
            with_ac_avg_price += price
            num_houses_with_ac += 1
        else:
            without_ac_avg_price += price
            
    with_hotwater_avg_price /= num_houses_with_hotwater
    without_hotwater_avg_price /= (len(data) - num_houses_with_hotwater)
    
    with_ac_avg_price /= num_houses_with_ac
    without_ac_avg_price /= (len(data) - num_houses_with_ac)
    
    x = [with_hotwater_avg_price, without_hotwater_avg_price, with_ac_avg_price, without_ac_avg_price]
    y = ['With Hot Water Heating', 'Without Hot Water Heating', 'With AC', 'Without AC']
    
    fig = plt.figure(figsize = (12, 6))
    bar_list = plt.bar(y, x)
    bar_list[0].set_color('r')
    bar_list[1].set_color('r')
    plt.ylabel('Average Price (In Millions of Dollars)')
    plt.show()


        
def train():
    data = load_file_data()
    network = genetic_algorithm(88, 500, data)
    #network = load_network('best_network_weights.txt')
    print('====================================')
    print('BEST NETWORK FITNESS: ', network.fitness)

    avg_diff = 0.0
    avg_percent_accurate = 0.0
    for index in range(len(data)):
        output = network.feed_forward(data[index][1:])
        print('Output: ', output[0], '    Desired Output: ', data[index][0])
        diff = abs(output[0] - data[index][0])
        avg_diff += diff
        avg_percent_accurate += min(output[0], data[index][0]) / max(output[0], data[index][0])
    avg_diff /= len(data)
    avg_percent_accurate /= len(data)
    print('Avg Diff: ', avg_diff)  
    print('Avg Percentage Accurate: ', avg_percent_accurate)  

    write_weights_file(network)

def run_ui():
    network = load_network('best_network_weights8645.txt')   # load a pre-trained network
    # loop until user exits
    while True:
        # show options
        print('\nType the number of one of the following options:')
        print('1) Make a house price prediction using the trained model.')
        print('2) Show scatter plot of price compared to square footage.')
        print('3) Show pie chart of distribution of houses by number of stories.')
        print('4) Show bar chart of average house price with and without hot water heating, and with or without AC.')
        print('5) Exit.')
        
        # make choice
        while True:
            try:
                choice = int(input('Selection: '))
                if not 1 <= choice <=5:
                    raise ValueError
                break   # leave infinite loop if user enters a valid number
            except ValueError:
                print('Please enter a number between 1 and 5.')
                
        match choice:
            case 1:
                input_data = []
                print('\nYou will now enter house information so a price prediction can be made.')
                while True:
                    try:
                        sqft = float(input('Enter the square footage of the house: ').strip())
                        if sqft <= 0:
                            raise ValueError
                        sqfoot_normalized = (sqft - 1650.0) / (15600.0 - 1650.0)
                        input_data.append(sqfoot_normalized)
                        break
                    except ValueError:
                        print('Enter a valid number please.')
                
                while True:
                    try:
                        num_bedrooms = int(input('Enter the number of bedrooms in the house: ').strip())
                        if num_bedrooms <= 0:
                            raise ValueError
                        input_data.append(num_bedrooms)
                        break
                    except ValueError:
                        print('Enter a valid number please.')
                        
                while True:
                    try:
                        num_bathrooms = int(input('Enter the number of bathrooms in the house: ').strip())
                        if num_bathrooms <= 0:
                            raise ValueError
                        input_data.append(num_bathrooms)
                        break
                    except ValueError:
                        print('Enter a valid number please.')
                        
                while True:
                    try:
                        num_stories = int(input('Enter the number of stories the house has: ').strip())
                        if num_stories <= 0:
                            raise ValueError
                        input_data.append(num_stories)
                        break
                    except ValueError:
                        print('Enter a valid number please.')
                    
                while True:
                    try:
                        has_mainroad_access_str = input('Does this house have mainroad access? Type yes or no: ').strip().lower()
                        if not has_mainroad_access_str == 'yes' and not has_mainroad_access_str == 'no':
                            raise ValueError
                        has_mainroad_access = 1 if has_mainroad_access_str == 'yes' else 0
                        input_data.append(has_mainroad_access)
                        break
                    except ValueError:
                        print('Please enter yes or no.')
                        
                while True:
                    try:
                        has_guestroom_str = input('Does this house have a guestroom? Type yes or no: ').strip().lower()
                        if not has_guestroom_str == 'yes' and not has_guestroom_str == 'no':
                            raise ValueError
                        has_guestroom = 1 if has_guestroom_str == 'yes' else 0
                        input_data.append(has_guestroom)
                        break
                    except ValueError:
                        print('Please enter yes or no.')
                        
                while True:
                    try:
                        has_basement_str = input('Does this house have a basement? Type yes or no: ').strip().lower()
                        if not has_basement_str == 'yes' and not has_basement_str == 'no':
                            raise ValueError
                        has_basement = 1 if has_basement_str == 'yes' else 0
                        input_data.append(has_basement)
                        break
                    except ValueError:
                        print('Please enter yes or no.')
                        
                while True:
                    try:
                        has_hotwaterheating_str = input('Does this house have hot water heating? Type yes or no: ').strip().lower()
                        if not has_hotwaterheating_str == 'yes' and not has_hotwaterheating_str == 'no':
                            raise ValueError
                        has_hotwaterheating = 1 if has_hotwaterheating_str == 'yes' else 0
                        input_data.append(has_hotwaterheating)
                        break
                    except ValueError:
                        print('Please enter yes or no.')
                        
                while True:
                    try:
                        has_ac_str = input('Does this house have air conditioning? Type yes or no: ').strip().lower()
                        if not has_ac_str == 'yes' and not has_ac_str == 'no':
                            raise ValueError
                        has_ac = 1 if has_ac_str == 'yes' else 0
                        input_data.append(has_ac)
                        break
                    except ValueError:
                        print('Please enter yes or no.')
                        
                while True:
                    try:
                        num_parking_spaces = int(input('Enter the number of parking spaces the house has: ').strip())
                        if num_parking_spaces < 0:
                            raise ValueError
                        input_data.append(num_parking_spaces)
                        break
                    except ValueError:
                        print('Enter a valid number please.')
                        
                while True:
                    try:
                        in_preferred_area_str = input('Is this house in a preferred area? Type yes or no: ').strip().lower()
                        if not in_preferred_area_str == 'yes' and not in_preferred_area_str == 'no':
                            raise ValueError
                        in_preferred_area = 1 if in_preferred_area_str == 'yes' else 0
                        input_data.append(in_preferred_area)
                        break
                    except ValueError:
                        print('Please enter yes or no.')
                        
                while True:
                    try:
                        furnished_info_str = input('Is this house furnished, semi-furnished, or not furnished? Type furnished, semi-furnished, or not furnished: ').strip().lower()
                        if not furnished_info_str == 'furnished' and not furnished_info_str == 'semi-furnished' and not furnished_info_str == 'not furnished':
                            raise ValueError
                        furnished_info = -1.0
                        if furnished_info_str == 'furnished':
                            furnished_info = 1.0
                        elif furnished_info_str == 'semi-furnished':
                            furnished_info = 0.5
                        else:
                            furnished_info = 0.0
                        input_data.append(furnished_info)
                        break
                    except ValueError:
                        print('Please enter furnished, semi-furnished, or not furnished.')
                        
                final_input_data = np.array(input_data)    
                prediction = network.feed_forward(final_input_data)[0] * 1000000.0
                print('Predicted house price: ', '${:,.2f}'.format(prediction))
                
                
            case 2:
                scatter_plot()
            case 3:
                pie_chart()
            case 4:
                bar_chart()
            case 5:
                return
        
run_ui()